<?php

$codes = include('codes.php');

if (isset($_POST['code']) && in_array($_POST['code'], $codes)) {
	setcookie("AccessGranted", $_POST['code'], time()+60*60*24*30);
}
header('Location: /');
die();