<?php
date_default_timezone_set("UTC");

$content = file_get_contents("events.json");
$events = json_decode($content, true, 10, JSON_OBJECT_AS_ARRAY);

function date_occurence($occurence) {
  $basedate = 1537747800 + ($occurence*24*60*60);
  $occurences = 14;
  return $basedate + ($occurences*24*60*60) * ceil((time() - $basedate) / ($occurences*24*60*60));
}
foreach($events as $key=>$event) {
    $events[$key]['date'] = date_occurence($event['c']);
}
usort($events, function($a, $b) {
    return $a['date'] >= $b['date'];
});

$days = [];
foreach($events as $event) {
    $eday = date_occurence($event['c']);
    $afterday = ceil(($eday - time()) / (24*60*60));
    $days[$afterday] = $eday;
}
ksort($days);

$lasteventidx = count($events) - 1;
$todayEvents = [];
for ($i = $lasteventidx; $i > 0; $i--) {
    if ($events[$lasteventidx]['c'] != $events[$i]['c'])
        break;
    $todayEvents[] = $events[$i];
}
$todayEvents = array_reverse($todayEvents);

$accessCodes = include('codes.php');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>D&amp;M</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
    <style type="text/css">
        html, body {
            font-size: 12px;
        }
        h1 {
            font-size: 2em;
        }
        select {
            color: black;
        }
        table label {
            display: inline-block;
            margin-right: 10px;
        }
        button {
            color: black;
        }
        .table>thead>tr+tr>th {
            border-bottom: 2px solid #2b1a00;
        }
        .rfirst td {
            border-bottom: 1px solid #7b4800;
            height: 0px;
            padding: 0 !important;
        }
    </style>
</head>
<body>
    <div class="container-fluid">

        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-2">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-8">
                    <h1>G&amp;G Events Calendar</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-2">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-8">
                <?php if (!$_COOKIE["AccessGranted"] || !in_array($_COOKIE["AccessGranted"], $accessCodes)) : ?>

                    <form action="check.php" method="post">
                        <label>Please insert the access code: <input type="text" name="code"></label>
                        <input type="submit" name="Submit">
                    </form>
                    <p><small>(In case you've lost your access 'code' please contact me in game. Kardier)</small></p>

                <?php else : ?>

                    <table id="events-list" class="table table-hover">
                        <thead>
                            <tr>
                                <td colspan="5"><h4>Today's events</h4></td>
                            </tr>
                            <?php foreach ($todayEvents as $event) : ?>
                            <tr>
                                <td style="white-space: nowrap;"><a><?php echo $event['type'] ?></a></td>
                                <td><a><?php echo $event['ttype'] ?></a></td>
                                <td><a><?php echo $event['name'] ?></a></td>
                                <td colspan="2"></td>
                            </tr>
                            <?php endforeach ?>
                            <tr class="rfirst">
                                <td colspan="5" style="height: auto"></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <label>Filter:</label>
                                    <label><input type="checkbox" name="type" value="ally" /> ALLY</label>
                                    <label><input type="checkbox" name="type" value="single" /> SINGLE</label>
                                    <label>AFTER:
                                        <select name="day-filter">
                                            <option value=""></option>
                                            <?php foreach($days as $key=>$day) : ?>
                                            <option value="<?php echo $day ?>"><?php echo $key ?> days</option>
                                            <?php endforeach ?>
                                        </select>
                                    </label>
                                    <label>
                                        <select name="ttype-filter">
                                            <option value="">All kind of tasks</option>
                                            <option value="QUESTS">Quests</option>
                                            <option value="EDICTS">Edicts</option>
                                            <option value="POWER">Power</option>
                                            <option value="RES GATHERING">Res Gathering</option>
                                            <option value="ATTACK">Attack Enemies</option>
                                            <option value="RECRUIT">Recruit Units</option>
                                            <option value="PORT">Port</option>
                                            <option value="CRAFTING">Blacksmithing</option>
                                            <option value="HUNT">Monster Hunt</option>
                                        </select>
                                    </label>
                                    <button id="resetFilterButton">RESET</button>
                                </td>
                            </tr>
                            <tr>
                                <th>Type</th>
                                <th>Task</th>
                                <th>Event</th>
                                <th>Day</th>
                                <th>Starts</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $g = null; foreach ($events as $key=>$event) : if (empty($event['type'])) continue; ?>
                            <?php if ($g != $event['c']) : $g = $event['c']; ?>
                            <tr class="rfirst cd<?php echo strtolower($event['date']) ?>">
                                <td colspan="5"></td>
                            </tr>
                            <?php endif ?>
                            <tr class="idx<?php echo $key ?> <?php echo strtolower($event['type']) ?> c<?php echo $event['c'] ?> cd<?php echo strtolower($event['date']) ?>">
                                <td style="white-space: nowrap;"><a><?php echo $event['type'] ?></a></td>
                                <td><?php echo $event['ttype'] ?></td>
                                <td><?php echo $event['name'] ?></td>
                                <td><?php echo date('D', $event['date']) ?></td>
                                <td id="eventTime<?php echo $key ?>" style="white-space: nowrap;text-align: right;"></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>

                <?php endif ?>
                </div>
            </div>
        </div>

    <footer>
        <div class="row">
            <div class="col-lg-12">

            </div>
        </footer>

    </div>

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/Timer.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            <?php foreach ($events as $key=>$event) : ?>
            if (document.getElementById('eventTime<?php echo $key ?>'))
            new Timer(document.getElementById('eventTime<?php echo $key ?>'), new Date(<?php echo $event['date'] ?> * 1000)).start();
            <?php endforeach ?>

            var eventsList = {
                type: null,
                ttype: null,
                day: null,
                refresh: function() {
                    var that = this;
                    $('#events-list tbody tr').each(function() {
                        if (!$(this).find('td').get(1) || (!that.type || $(this).hasClass(that.type)) 
                            && (!that.day || $(this).hasClass('cd'+that.day))
                            && (!that.ttype || $(this).find('td').get(1).innerText == that.ttype))
                            $(this).show();
                        else
                            $(this).hide();
                    });
                },
                reset: function() {
                    this.type = this.ttype = this.day = null;
                }
            }

            var filterByType = [];
            $('input[name=type]').change(function() {
                if (this.checked) {
                    if (filterByType.indexOf(this.value) == -1)
                        filterByType.push(this.value)
                }
                else {
                    if (filterByType.indexOf(this.value) !== -1)
                        filterByType.splice(filterByType.indexOf(this.value), 1);
                }

                eventsList.type = filterByType.length == 0 || filterByType.length > 1 ? null : filterByType[0];

                eventsList.refresh();
            });

            $('select[name=ttype-filter]').change(function() {
                eventsList.ttype = this.value || null;
                eventsList.refresh();
            });

            $('select[name=day-filter]').change(function() {
                eventsList.day = this.value || null;
                eventsList.refresh();
            });

            function resetFilter() {
                $('select[name=ttype-filter]').val('');
                $('select[name=day-filter]').val('');
                $('input[name=type]:checked').attr('checked', false);
                filterByType = [];
                eventsList.reset();
                eventsList.refresh();
            }
            $('#resetFilterButton').click(resetFilter);
        })
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-98639495-1', 'auto');
        <?php if (isset($_COOKIE["AccessGranted"])) : ?>
        ga('send', 'pageview', '/?code=<?php echo preg_replace('/[^0-9a-z]+/i', '', $_COOKIE["AccessGranted"]) ?>');
        <?php else : ?>
        ga('send', 'pageview');
        <?php endif ?>
    </script>
</html>