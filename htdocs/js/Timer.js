function Timer(view, date) {
    this.view = view;
    this.timer = date;
}

Timer.prototype.tick = function() {
    var formattedTime = this.getFormattedTime();
    if (this.view.innerHTML != formattedTime)
        this.view.innerHTML = formattedTime;
    if (formattedTime == "00:00")
        this.view.style.color = "#aeaeae";
}

Timer.prototype.getFormattedTime = function() {
    var sdiff = Math.floor((this.timer.getTime() - new Date().getTime()) / 1000);
    
    if (sdiff < 0)
        return "00:00";

    var fdate = "";
    
    var d = Math.floor(sdiff / (24 * 60 * 60));
    if (d > 0) {
        fdate += d + "d ";
    }
    
    var h = Math.floor(sdiff % (24 * 60 * 60) / (60 * 60));
    if (h > 0)
        fdate += h + ":";
    fdate += ("0" + Math.floor(sdiff % (24 * 60 * 60) % (60 * 60) / 60)).substr(-2);
    // fdate += ":" + ("0" + Math.floor(sdiff % (24 * 60 * 60) % (60 * 60) % 60)).substr(-2);

    return fdate.replace(/([0-9]+d)*([^:]+:[^:]+)/, "<a>$1</a>$2");
}

Timer.prototype.start = function() {
    if (this._timer)
        clearInterval(this._timer);

    var that = this;
    this._timer = setInterval(function() { that.tick() }, 1000);
}

/**
 * Use Case Example:
 * 
 * var t1 = new Timer(document.getElementById('timer1'), new Date(new Date().getTime() + 100000 * 1000)).start();
 */
